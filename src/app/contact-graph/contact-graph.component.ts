import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid
} from "ng-apexcharts";
import { CommonMethodsService } from "../common-methods.service";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-contact-graph',
  templateUrl: './contact-graph.component.html',
  styleUrls: ['./contact-graph.component.scss']
})
export class ContactGraphComponent implements OnInit {
  dateSelector: FormGroup;
  tableData: any[] = [];
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(private _formBuilder: FormBuilder,
    private _commonService: CommonMethodsService) {
    this.chartOptions = {
      series: [
        {
          name: "Desktops",
          data: []
        }
      ],
      chart: {
        height: 350,
        type: "line",
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },
      title: {
        text: "Contact Us Count Date Wise",
        align: "right"
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      xaxis: {
        categories: [

        ]
      }
    };
  }
  ngOnInit() {
    this.dateSelector = this._formBuilder.group({
      startDate: [''],
      endDate: ['']
    })
  }

  onSubmit() {
    const startDate = this.dateSelector.get('startDate').value;
    const endDate = this.dateSelector.get('endDate').value;
    this._commonService.getGraphDetails(startDate, endDate)
      .pipe()
      .subscribe(data => {
        this.tableData = JSON.parse(JSON.stringify(data));
        const dataLength = JSON.parse(JSON.stringify(data)).length;
        for (var i = 0; i < dataLength; i++) {
          this.chartOptions.series[0].data.push(data[i].count);
          this.chartOptions.xaxis.categories.push(data[i].date)
        }

      })
    console.log("dssadsadsadadada", this.chartOptions.series[0]);
    console.log("dssadsadsadadada", this.chartOptions.xaxis.categories);

  }

}



