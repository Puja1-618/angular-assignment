import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactGraphComponent } from './contact-graph.component';

describe('ContactGraphComponent', () => {
  let component: ContactGraphComponent;
  let fixture: ComponentFixture<ContactGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
