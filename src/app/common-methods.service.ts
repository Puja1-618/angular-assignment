import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonMethodsService {

  constructor(private _httpClient: HttpClient) { }
  addContacts(formData) {
    return this._httpClient.post<any>('https://6e82bd6374b0.ngrok.io/contactus/', {
      "first_name": formData.firstName,
      "last_name": formData.lastName,
      "email": formData.email,
      "message": formData.message
    })
  }

  getContacts() {
    return this._httpClient.get('https://6e82bd6374b0.ngrok.io/contactus')
  }

  getGraphDetails(startDate, endDate) {
    return this._httpClient.get('https://6e82bd6374b0.ngrok.io/analytical/?start_date=' + startDate + '&end_date=' + endDate)
  }
}
