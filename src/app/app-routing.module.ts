import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddContactComponent } from './add-contact/add-contact.component';
import { ContactGraphComponent } from './contact-graph/contact-graph.component';
import { LoginPageComponent } from './login-page/login-page.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'contact-us',
    component: AddContactComponent
  },
  {
    path: 'graph',
    component: ContactGraphComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'contact-us'
  },
  {
    path: '**',
    redirectTo: 'contact-us'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
