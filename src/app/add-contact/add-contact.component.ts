import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonMethodsService } from '../common-methods.service';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {
  contactUs: FormGroup;
  constructor(private _formBuilder: FormBuilder, private _commonService: CommonMethodsService, private _router: Router) {
    this.contactUs = this._formBuilder.group({
      firstName: [''],
      lastName: [''],
      email: [''],
      message: ['']
    })
  }

  ngOnInit(): void {
    this._commonService.getContacts()
    .pipe()
    .subscribe(data =>{
      console.log("data",data);
      
    })
  }

  onSubmit() {
    const formObject = {
      firstName: this.contactUs.get('firstName').value,
      lastName: this.contactUs.get('lastName').value,
      email: this.contactUs.get('email').value,
      message: this.contactUs.get('message').value,
    }
    this._commonService.addContacts(formObject)
      .pipe()
      .subscribe(data => {
        this._router.navigate(['/graph'])
      }, err => {
        this._router.navigate(['/login'])
      })

  }
}
